const express = require('express')
const promisify = require('util').promisify
const app = express()
const fs = require('fs')
const _ = require('lodash/fp')
const path = require('path')
const shuffler = require('shuffle-seed')

const imgPaths = process.argv.slice(2)
if (!imgPaths.length) throw new Error('usage: node main.js <imgPath...>')
function flatten(list) {
  return [].concat.apply([], list)
}
function pathTree(root, tail=null) {
  const branch = tail ? path.join(root, tail) : root
  return promisify(fs.stat)(branch).then(stats => {
    const entry = {path: branch, root, tail, stats}
    if (stats.isDirectory()) {
      return promisify(fs.readdir)(branch).then(twigs => {
        return Promise.all(twigs.map(twig =>
          pathTree(root, tail ? path.join(tail, twig) : twig)
        ))
        .then(flatten)
        .then(fs => [entry].concat(fs))
      })
    } else {
      return [entry]
    }
  })
}
const fileTree = (root, tail) =>
  pathTree(root, tail).then(_.filter(f => !f.stats.isDirectory()))
const dirTree = (root, tail) =>
  pathTree(root, tail).then(_.filter(f => f.stats.isDirectory()))

function structuredSelect(fns, consts) {
  // like reselect's createStructuredSelector, but without caching
  return val => Object.assign({}, consts, _.mapValues(fn => fn(val), fns))
}
function unwrap(default_, fn, val) {
  return val == null ? default_ : fn(val)
}
function clamp(min, max) {
  return val => Math.min(max, Math.max(min, val))
}
const fileList = _.memoize(function _fileList(paths) {
  return Promise.all(paths.map(path => fileTree(path)))
  .then(_.flow(
    _.flatten,
    // always skip hidden files - starting with a dot
    _.filter(f => !/(^|\/|\.)\.[^\/]/.test(f.path)),
  ))
})
const search = _.memoize(function _search(q) {
  const searcher = unwrap(_.identity, q => {
    const re = new RegExp(q)
    return _.filter(f => re.test(f.path))
  }, q)
  // return searcher
  return _.memoize(searcher)
})
const sort = _.memoize(function _sort(seed) {
  const sorter = unwrap(_.sortBy(f => -f.stats.mtime), seed => fs => shuffler.shuffle(fs, seed), seed)
  // return sorter
  return _.memoize(sorter)
})
function clearCache() {
  [fileList, search, sort].forEach(fn => fn.cache.clear())
}

app.use('/', express.static('./node_modules/@images/www/dist'))
imgPaths.forEach(imgPath => {
  app.use('/i', express.static(imgPath))
})
function watch(watchDir) {
  // console.log('watching', watchDir)
  fs.watch(watchDir, (eventType, triggerFile) => {
    console.log('watch:', eventType, triggerFile)
    clearCache()

    const triggerPath = path.join(watchDir, triggerFile)
    promisify(fs.stat)(triggerPath).then(stats => {
      if (stats.isDirectory()) {
        watch(triggerPath)
      }
    })
    // if a file's immediately removed (like .crdownload, for chrome downloads) don't whine
    .catch(e => {})
  })
}
// watch all directories for changes, including nested directories on linux where fs.watch(..., {recursive:true}) breaks.
Promise.all(imgPaths.map(path => dirTree(path)))
.then(_.flatten)
.then(imgPaths => imgPaths.forEach(f => watch(f.path)))

app.use(/^\/api/, (req, res, next) => {
  // https://enable-cors.org/server_expressjs.html
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

app.get('/api/images/:page', (req, res) => {
  const start = Date.now()
  const pageSize = unwrap(10, _.flow(parseInt, clamp(0, 100)), req.query.pageSize)
  const page = unwrap(0, parseInt, req.params.page)
  if (page < 0) return res.status(400).send({error: "page < 0"})
  return fileList(imgPaths)
  .then(search(req.query.q))
  .then(sort(req.query.seed))
  .then(structuredSelect({
    imageCount: _.size,
    pageCount: _.flow(_.size, n => n / pageSize, Math.ceil),
    images: _.flow(
      // _.map(f => '/i/'+f.tail),
      _.map(f => ({
        id: f.tail,
        path: f.path,
        // dir: path.basename(f.root),
        dir: f.root,
      })),
      _.slice(pageSize * page, pageSize * (page+1)),
    ),
  }, {
    dirs: imgPaths,
    page,
    pageSize,
    search: req.query.q || null,
    seed: req.query.seed || null,
  }))
  .then(json => Object.assign(json, {searchDurationMs: Date.now() - start}))
  .then(json => res.send(JSON.stringify(json, null, 2)))
  .catch(error => {
    console.error(error)
    res.status(500).send({error: error+''})
  })
})
app.post('/api/image/copy', (req, res) => {
  const src = req.query.src
  const dest = path.join(req.query.dest, path.basename(src))
  console.log('copy', src, dest)
  return promisify(fs.copyFile)(src, dest)
  .then(() => {
    res.send({src, dest, success: true})
  })
  .catch(error => {
    console.error(error)
    res.status(500).send({error: error+''})
  })
})
app.post('/api/image/delete', (req, res) => {
  const path = req.query.path
  console.log('delete', path)
  return promisify(fs.unlink)(path)
  .then(() => {
    res.send({path, success: true})
  })
  .catch(error => {
    console.error(error)
    res.status(500).send({error: error+''})
  })
})

app.listen(8000, () => console.log('Example app listening on port 8000, '+imgPaths))
