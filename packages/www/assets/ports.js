function debug(prefix) {
  return val => {
    console.debug(prefix, val)
    return val
  }
}
function qsStringify(qs) {
  var ret = Object.keys(qs)
  .filter(key => qs[key] != null)
  .map(key => encodeURIComponent(key)+'='+encodeURIComponent(qs[key]))
  .join('&')
  return ret ? '?'+ret : ''
}

var hostname = ''
var app = Elm.Main.fullscreen({hostname: hostname})
app.ports.imageListReq.subscribe(req => {
  const qs = qsStringify({pageSize: req.pageSize, q: req.search, seed: req.seed})
  const url = hostname+'/api/images/'+req.page+qs
  // console.log('imageListReq', req, url)
  fetch(debug("imageListReq")(url))
  .then(res => res.json())
  .then(debug("imageListRes"))
  .then(app.ports.imageListRes.send)
})
app.ports.imageCopyReq.subscribe(req => {
  const qs = qsStringify({src: req.src, dest: req.dest})
  const url = hostname+'/api/image/copy'+qs
  // console.log('imageListReq', req, url)
  fetch(debug("imageCopyReq")(url), {method: 'POST'})
  .then(res => res.json())
  .then(debug("imageCopyRes"))
  .then(app.ports.imageCopyRes.send)
})
app.ports.imageDeleteReq.subscribe(req => {
  if (confirm("Sure you want to delete "+req.path+"?")) {
    const qs = qsStringify({path: req.path})
    const url = hostname+'/api/image/delete'+qs
    // console.log('imageListReq', req, url)
    fetch(debug("imageDeleteReq")(url), {method: 'POST'})
    .then(res => res.json())
    .then(debug("imageDeleteRes"))
    .then(app.ports.imageDeleteRes.send)
  }
})
var hammer = new Hammer(document.documentElement)
// hammer.get('swipe').set({direction: Hammer.DIRECTION_ALL})
hammer
.on('swipeleft', event => {
  app.ports.rawSwipe.send('left')
})
.on('swiperight', event => {
  app.ports.rawSwipe.send('right')
})
