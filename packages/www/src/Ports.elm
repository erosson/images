port module Ports exposing (..)

import Json.Decode as Decode


type alias ImageListReq =
    { pageSize : Int, page : Int, search : Maybe String, seed : Maybe String }


port imageListReq : ImageListReq -> Cmd msg


type alias Image =
    { id : String, path : String, dir : String }


type alias ImageListRes =
    { imageCount : Int, dirs : List String, images : List Image, pageSize : Int, page : Int, search : Maybe String, seed : Maybe String }


port imageListRes : (ImageListRes -> msg) -> Sub msg


port imageCopyReq : { src : String, dest : String } -> Cmd msg


port imageCopyRes : ({ src : String, dest : String } -> msg) -> Sub msg


port imageDeleteReq : { path : String } -> Cmd msg


port imageDeleteRes : ({ path : String } -> msg) -> Sub msg


port rawSwipe : (String -> msg) -> Sub msg


type SwipeDir
    = Left
    | Right


parseSwipe : String -> SwipeDir
parseSwipe dir =
    case dir of
        "left" ->
            Left

        "right" ->
            Right

        _ ->
            Debug.crash "invalid swipe dir"


swipe : (SwipeDir -> msg) -> Sub msg
swipe msg =
    rawSwipe (parseSwipe >> msg)
