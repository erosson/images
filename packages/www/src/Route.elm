module Route exposing (Route(..), ImageListArgs, ImageShowArgs, RandomImageArgs, parse, stringify, href)

import Regex
import Navigation
import UrlParser as P exposing ((</>), (<?>))
import Http
import Maybe.Extra
import Html as H
import Html.Attributes as A


type alias ImageListArgs =
    { page : Int, search : Maybe String, seed : Maybe String }


type alias ImageShowArgs =
    { index : Int, search : Maybe String, seed : Maybe String }


type alias RandomImageArgs =
    { search : Maybe String, seed : Maybe String }


type Route
    = NotFound
    | ImageList ImageListArgs
    | ImageShow ImageShowArgs
    | RandomImage RandomImageArgs


parse : Navigation.Location -> Route
parse loc =
    loc
        |> hashQS
        |> P.parseHash parser
        |> Maybe.withDefault NotFound
        |> Debug.log "Route.parse"


hashQS : Navigation.Location -> Navigation.Location
hashQS loc =
    -- UrlParser doesn't do ?query=strings in the #hash, so fake it using the non-hash querystring
    case Regex.split (Regex.AtMost 1) (Regex.regex "\\?") loc.hash of
        [ hash ] ->
            { loc | search = loc.search }

        [ hash, qs ] ->
            { loc | hash = hash, search = loc.search ++ "&" ++ qs }

        [] ->
            Debug.crash "hashqs: empty"

        other ->
            Debug.crash "hashqs: 3+"


qsParser path =
    path <?> P.stringParam "q" <?> P.stringParam "seed"


parser : P.Parser (Route -> a) a
parser =
    P.oneOf
        [ P.map ImageList <| P.map (ImageListArgs 0) <| qsParser P.top
        , P.map ImageList <| P.map ImageListArgs <| qsParser <| P.s "images" </> P.int
        , P.map ImageShow <| P.map ImageShowArgs <| qsParser <| P.s "image" </> P.int
        , P.map RandomImage <| P.map RandomImageArgs <| qsParser <| P.s "random"
        ]


encodeQS : List ( String, Maybe String ) -> String
encodeQS pairs0 =
    let
        pairs : List ( String, String )
        pairs =
            pairs0
                |> List.map (\( k, v ) -> Maybe.map (\v -> ( k, v )) v)
                |> Maybe.Extra.values
    in
        if List.isEmpty pairs then
            ""
        else
            pairs
                |> List.map (\( k, v ) -> Http.encodeUri k ++ "=" ++ Http.encodeUri v)
                |> String.join "&"
                |> (++) "?"


stringify : Route -> String
stringify route =
    case route of
        NotFound ->
            "#/404"

        ImageList req ->
            (if req.page == 0 then
                "#/"
             else
                "#/images/" ++ toString req.page
            )
                ++ encodeQS
                    [ ( "q", req.search )
                    , ( "seed", req.seed )
                    ]

        ImageShow req ->
            "#/image/"
                ++ toString req.index
                ++ encodeQS
                    [ ( "q", req.search )
                    , ( "seed", req.seed )
                    ]

        RandomImage req ->
            "#/random"
                ++ encodeQS
                    [ ( "q", req.search )
                    , ( "seed", req.seed )
                    ]


href : Route -> H.Attribute msg
href =
    A.href << stringify
