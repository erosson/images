module Main exposing (..)

import Html as H
import Html.Attributes as A
import Html.Events as E
import Task
import Json.Decode as Decode
import Time as Time exposing (Time)
import Dict as Dict exposing (Dict)
import Maybe.Extra
import Regex
import Random
import Navigation
import Route as Route exposing (Route)
import Ports


main =
    -- H.program
    Navigation.programWithFlags NavigateTo
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type Msg
    = NavigateTo Navigation.Location
    | ModifyUrl Route
    | RandomSeedReq
    | RandomSeedRes (Maybe String)
    | Search { search : Maybe String }
    | ImageListRes Ports.ImageListRes
    | Swipe Ports.SwipeDir
    | ImageCopyReq Image String
    | ImageCopyRes
    | ImageDeleteReq Image
    | ImageDeleteRes


pageSize : Int
pageSize =
    20


type alias Image =
    { id : String, path : String, dir : String }


type alias Flags =
    { hostname : String }


type alias Model =
    { hostname : String
    , imageCount : Int
    , images : Dict Int (List Image)
    , route : Route
    , dirs : List String
    }


init0 : Flags -> Route -> ( Model, Cmd Msg )
init0 flags route =
    let
        model =
            { hostname = flags.hostname
            , imageCount = 0
            , images = Dict.empty
            , route = route
            , dirs = []
            }
    in
        ( model, fetchRoute model route )


randomImage : Route.RandomImageArgs -> Int -> Random.Generator Route
randomImage req imageCount =
    Random.int 0 imageCount
        |> Random.map (\index -> Route.ImageShow { index = index, search = req.search, seed = req.seed })


fetchRoute : Model -> Route -> Cmd Msg
fetchRoute model route =
    let
        fetchIf pred fetch =
            if pred then
                fetch
            else
                Cmd.none

        fetchOnePage page =
            fetchIf <|
                (page >= 0)
                    && (not <|
                            Dict.member page model.images
                                && (search model.route == search route)
                                && (seed model.route == seed route)
                       )

        fetchPages : Int -> (Int -> Cmd msg) -> Cmd msg
        fetchPages page fetch =
            Cmd.batch <| List.map (\p -> fetchOnePage p (fetch p)) [ page, page - 1, page + 1 ]
    in
        case route of
            Route.ImageList req ->
                fetchPages req.page <| \p -> Ports.imageListReq { page = p, search = req.search, seed = req.seed, pageSize = pageSize }

            Route.ImageShow req ->
                fetchPages (req.index // pageSize) <| \p -> Ports.imageListReq { page = p, search = req.search, seed = req.seed, pageSize = pageSize }

            Route.RandomImage req ->
                Random.generate ModifyUrl (randomImage req model.imageCount)

            _ ->
                Cmd.none


init : Flags -> Navigation.Location -> ( Model, Cmd Msg )
init flags =
    init0 flags << Route.parse


image : Int -> Model -> Maybe Image
image index model =
    if index < 0 then
        Nothing
    else
        imagePage (index // pageSize) model
            |> Maybe.withDefault []
            |> List.drop (index % pageSize)
            |> List.head


imageSrc : String -> Image -> String
imageSrc hostname image =
    hostname ++ "/i/" ++ image.id


imagePage : Int -> Model -> Maybe (List Image)
imagePage p =
    .images >> Dict.get p


type PlayerType
    = ImagePlayer
    | VideoPlayer


playerType : Image -> PlayerType
playerType image =
    if Regex.contains (Regex.caseInsensitive <| Regex.regex "\\.(webm|mp4)$") image.id then
        VideoPlayer
    else
        ImagePlayer


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NavigateTo loc ->
            let
                route =
                    Route.parse loc
            in
                ( { model | route = route }, fetchRoute model route )

        ModifyUrl route ->
            ( model, Navigation.modifyUrl <| Route.stringify route )

        ImageListRes res ->
            ( { model
                | imageCount = res.imageCount
                , dirs = res.dirs
                , images =
                    Dict.insert res.page
                        res.images
                        -- clear cached images if they've changed
                        (if model.imageCount == res.imageCount && search model.route == res.search then
                            model.images
                         else
                            Dict.empty
                        )
              }
            , Cmd.none
            )

        Swipe dir ->
            case model.route of
                Route.ImageShow req ->
                    let
                        route =
                            case dir of
                                Ports.Left ->
                                    imageIncr model.imageCount req 1

                                Ports.Right ->
                                    imageIncr model.imageCount req -1
                    in
                        ( model, Navigation.modifyUrl <| Route.stringify route )

                Route.ImageList req ->
                    let
                        route =
                            case dir |> Debug.log "dir" of
                                Ports.Left ->
                                    pageIncr (pageCount model.imageCount) req 1

                                Ports.Right ->
                                    pageIncr (pageCount model.imageCount) req -1
                    in
                        ( model, Navigation.modifyUrl <| Route.stringify route )

                _ ->
                    ( model, Cmd.none )

        Search query ->
            case model.route of
                Route.ImageList req ->
                    ( model, Navigation.modifyUrl <| Route.stringify <| Route.ImageList { req | search = query.search } )

                Route.ImageShow req ->
                    ( model, Navigation.modifyUrl <| Route.stringify <| Route.ImageShow { req | search = query.search } )

                _ ->
                    ( model, Cmd.none )

        RandomSeedReq ->
            ( model, Task.perform (RandomSeedRes << Just << toString) Time.now )

        RandomSeedRes seed ->
            case model.route of
                Route.ImageList req ->
                    ( model, Navigation.modifyUrl <| Route.stringify <| Route.ImageList { req | seed = seed } )

                Route.ImageShow req ->
                    ( model, Navigation.modifyUrl <| Route.stringify <| Route.ImageShow { req | seed = seed } )

                _ ->
                    ( model, Cmd.none )

        ImageCopyReq img dir ->
            let
                _ =
                    Debug.log "copyImage" ( img, dir )
            in
                ( model, Ports.imageCopyReq { src = img.path, dest = dir } )

        ImageDeleteReq img ->
            let
                _ =
                    Debug.log "deleteImage" img
            in
                ( model, Ports.imageDeleteReq { path = img.path } )

        ImageCopyRes ->
            let
                m =
                    { model | images = Dict.empty }
            in
                ( m
                , case model.route of
                    Route.ImageShow req ->
                        Navigation.modifyUrl <| Route.stringify <| Route.ImageShow { req | index = 0 }

                    _ ->
                        fetchRoute m m.route
                )

        ImageDeleteRes ->
            let
                m =
                    { model | images = Dict.empty }
            in
                ( m, fetchRoute m m.route )


search : Route -> Maybe String
search route =
    case route of
        Route.ImageList req ->
            req.search

        Route.ImageShow req ->
            req.search

        _ ->
            Nothing


seed : Route -> Maybe String
seed route =
    case route of
        Route.ImageList req ->
            req.seed

        Route.ImageShow req ->
            req.seed

        _ ->
            Nothing


subscriptions model =
    Sub.batch
        [ Ports.imageListRes ImageListRes
        , Ports.swipe Swipe
        , Ports.imageCopyRes (always ImageCopyRes)
        , Ports.imageDeleteRes (always ImageDeleteRes)
        ]


imageIncr : Int -> Route.ImageShowArgs -> Int -> Route
imageIncr max_ req incr =
    Route.ImageShow { req | index = clamp 0 (max_ - 1) <| req.index + incr }


pageIncr : Int -> Route.ImageListArgs -> Int -> Route
pageIncr max_ req incr =
    Route.ImageList { req | page = clamp 0 (max_ - 1) <| req.page + incr }


pageCount : Int -> Int
pageCount imageCount =
    ceiling <| toFloat imageCount / toFloat pageSize


view : Model -> H.Html Msg
view model =
    case model.route of
        Route.NotFound ->
            H.div [] [ H.text "404" ]

        Route.RandomImage req ->
            H.div [] [ H.text "rolling..." ]

        Route.ImageList req ->
            case imagePage req.page model of
                Nothing ->
                    H.div [ A.class "image-list" ] [ H.text "No more images :(" ]

                Just images ->
                    H.div [ A.class "image-list" ]
                        [ viewSearchForm req
                        , viewPager model.imageCount req
                        , H.div [ A.class "items" ] (List.indexedMap (viewImageListItem model.hostname req) images)
                        , viewPager model.imageCount req
                        ]

        Route.ImageShow req ->
            case image req.index model of
                Nothing ->
                    H.div [] [ H.text "No such image :(" ]

                Just img ->
                    let
                        src =
                            imageSrc model.hostname img

                        prevRoute =
                            imageIncr model.imageCount req -1

                        nextRoute =
                            imageIncr model.imageCount req 1

                        prefetch index =
                            Maybe.map
                                (\img ->
                                    H.node "link"
                                        [ A.rel "prefetch"
                                        , A.attribute "data-index" <| toString index
                                        , A.href <| imageSrc model.hostname img
                                        ]
                                        []
                                )
                                (image index model)

                        prefetchNext index n =
                            Maybe.Extra.values <| List.map prefetch <| List.range (index - n) (index - 1) ++ List.range (index + 1) (index + n)

                        tr header body =
                            H.tr [] [ H.th [] header, H.td [] body ]
                    in
                        H.div [ A.class "main" ]
                            [ H.div [ onClickPreventDefault <| ModifyUrl nextRoute ]
                                [ (case playerType img of
                                    ImagePlayer ->
                                        H.img [ A.src src ] []

                                    VideoPlayer ->
                                        -- next/prev change the source, but not the video tag itself - not enough to actually change the playing video.
                                        H.video [ A.class <| "video-" ++ toString req.index, A.controls True, A.autoplay True, A.loop True, A.src src ] []
                                  )
                                ]

                            -- modifyUrl: try not to add a history entry; back should go to the list
                            -- , H.a [ A.class "prev", onClickPreventDefault <| ModifyUrl prevRoute, Route.href prevRoute ] [ H.text "prev" ]
                            , H.a [ A.class "next", onClickPreventDefault <| ModifyUrl nextRoute, Route.href nextRoute ] [ H.text "next" ]
                            , H.table [ A.class "form" ]
                                [ tr [ H.text "Id" ] [ H.text img.id ]
                                , tr [ H.text "Path" ] [ H.text img.path ]
                                , tr [ H.text "Dir" ] [ H.text img.dir ]
                                , tr [ H.text "Copy to" ]
                                    (List.map
                                        (\dir ->
                                            H.button
                                                [ A.class "copy-button"
                                                , E.onClick <| ImageCopyReq img dir
                                                ]
                                                [ H.text dir ]
                                        )
                                        (List.filter ((/=) img.dir) model.dirs)
                                    )
                                , tr []
                                    [ H.button
                                        [ A.class "copy-button"
                                        , E.onClick <| ImageDeleteReq img
                                        ]
                                        [ H.text "Delete" ]
                                    ]
                                ]
                            , H.div [] <| prefetchNext req.index 3
                            ]


onClickPreventDefault : msg -> H.Attribute msg
onClickPreventDefault =
    E.onWithOptions "click" { stopPropagation = True, preventDefault = True } << Decode.succeed


viewSearchForm : Route.ImageListArgs -> H.Html Msg
viewSearchForm req =
    H.div []
        [ H.input
            [ A.value <| Maybe.withDefault "" req.search
            , E.onInput
                (\q ->
                    Search
                        { search =
                            if q == "" then
                                Nothing
                            else
                                Just q
                        }
                )
            ]
            []
        , H.label []
            [ H.input
                [ A.type_ "checkbox"
                , A.checked <| Maybe.Extra.isJust req.seed
                , E.onCheck
                    (\v ->
                        if v then
                            RandomSeedReq
                        else
                            RandomSeedRes Nothing
                    )
                ]
                []
            , H.text "Random sort"
            ]
        , H.a [ Route.href <| Route.RandomImage { search = req.search, seed = req.seed } ] [ H.text "Jump to random image" ]
        ]


viewPager : Int -> Route.ImageListArgs -> H.Html msg
viewPager imageCount req =
    H.div [ A.class "pager" ]
        [ H.a [ Route.href <| Route.ImageList { req | page = 0 } ] [ H.text "<< First" ]
        , H.a [ Route.href <| pageIncr (pageCount imageCount) req -1 ] [ H.text "< Prev" ]
        , H.span []
            [ H.text <|
                toString (min imageCount <| req.page * pageSize + 1)
                    ++ "-"
                    ++ toString (min imageCount <| req.page * pageSize + pageSize)
                    ++ " of "
                    ++ toString imageCount
            ]
        , H.a [ Route.href <| pageIncr (pageCount imageCount) req 1 ] [ H.text "Next >" ]
        , H.a [ Route.href <| Route.ImageList { req | page = (pageCount imageCount) - 1 } ] [ H.text "Last >>" ]
        ]


viewImageListItem : String -> Route.ImageListArgs -> Int -> Image -> H.Html msg
viewImageListItem hostname req pageIndex image =
    let
        index =
            req.page * pageSize + pageIndex

        src =
            imageSrc hostname image

        img =
            case playerType image of
                ImagePlayer ->
                    H.img [ A.src src ] []

                VideoPlayer ->
                    -- H.span [ ] [ H.text "(video)" ]
                    H.video [ A.controls False, A.autoplay False ] [ H.source [ A.src src ] [] ]
    in
        H.a
            [ A.class "item image-list-item"
            , A.href <| Route.stringify <| Route.ImageShow { index = index, search = req.search, seed = req.seed }
            , A.title image.path
            ]
            [ img

            -- , H.br [] []
            , H.div [ A.class "label" ] [ H.text <| image.dir ++ "::" ++ image.id ]
            ]
